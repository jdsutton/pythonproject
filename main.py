#!/usr/bin/env python3

from pythonlambdaserver.LambdaServer import LambdaServerApp

def main():
    '''
    TODO.
    '''


    directory = './services'
    port = 5001

    LambdaServerApp.run(
        [directory],
        port,
        allowedOrigins=[
            'http://127.0.0.1:5000',
            'http://localhost:5000',
            'http://0.0.0.0:5000',
        ],
    )

if __name__ == '__main__':
    main()
