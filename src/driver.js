import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();
    }
}

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    TEMPLATE: {
        
    }
};

const CLASS = {

};
